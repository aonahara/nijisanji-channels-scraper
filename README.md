# nijisanji-channels-scraper

Scrape a list of all Nijisanji YouTube channels from [https://www.nijisanji.jp/members/](https://www.nijisanji.jp/members/).

## How to use

```
yarn start
```

Member data will be fetched and saved in the `out/` folder.

import axios from "axios";
import * as fs from "fs";
import PromisePool from "@supercharge/promise-pool";

const getPageData = async (url: string) => {
  const pageString = (await axios.get(url).then((res) => res.data)) as string;
  const tagOpen = '<script id="__NEXT_DATA__" type="application/json">';
  const tagClose = "</script>";
  const idx = pageString.indexOf(tagOpen);
  const substr = pageString.substr(idx + tagOpen.length);
  const substr2 = substr.substr(0, substr.indexOf(tagClose));
  return JSON.parse(substr2);
};

type Liver = {
  slug: string;
  name: string;
  english_name: string;
  affiliation: string[];
  channel_id: string;
};

const fetchMember = async (liver: Liver) => {
  const { slug, name, english_name, affiliation } = liver;

  console.log("Fetching", english_name);
  const data = await getPageData("https://www.nijisanji.jp/members/" + slug);
  const youtubeChannel = data.props.pageProps.liver.social_links.youtube;
  const channel_id = youtubeChannel
    ? youtubeChannel.match(/[0-9a-zA-Z_-]{24}/)[0]
    : null;
  return {
    slug,
    name,
    english_name,
    channel_id,
    affiliation,
  };
};

const run = async () => {
  console.log("Fetching list");

  if (!fs.existsSync("./out")) fs.mkdirSync("./out");

  const home = await getPageData("https://www.nijisanji.jp/members");
  const concurrency = 8;

  console.log("Fetching member data with concurrency", concurrency);
  const { results } = await PromisePool.withConcurrency(concurrency)
    .for(home.props.pageProps.livers as Liver[])
    .process(fetchMember);

  console.log("Sorting");
  const livers = results
    .sort((a, b) => a.affiliation[0].localeCompare(b.affiliation[0]))
    .sort((a, b) => a.english_name.localeCompare(b.english_name));

  fs.writeFileSync(
    "./out/channels.ndjson",
    livers.map((l) => JSON.stringify(l)).join("\n")
  );

  const byAffiliation: { [key: string]: Liver[] } = {};
  for (const liver of livers) {
    const affs = liver.affiliation;
    for (const aff of affs) {
      if (!Array.isArray(byAffiliation[aff])) byAffiliation[aff] = [];
      byAffiliation[aff].push(liver);
    }
  }

  // Generate commented array
  fs.writeFileSync(
    "./out/youtube_channels.js",
    "[\n" +
      Object.keys(byAffiliation)
        .map(
          (aff) =>
            "  // " +
            aff +
            "\n" +
            byAffiliation[aff]
              .filter((liver) => !!liver.channel_id)
              .map((liver) =>
                [
                  '  "',
                  liver.channel_id,
                  '", // ',
                  liver.name,
                  " / ",
                  liver.english_name,
                ].join("")
              )
              .join("\n")
        )
        .join("\n") +
      "\n]"
  );

  console.log("All done!");
};

run();
